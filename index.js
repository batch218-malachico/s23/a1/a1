// console.log("Hello World");

/*Trainer Object*/
let pokemonTrainer ={

	name: 'Ashh Ketchum',
	age: 10,
	pokemon: ['Pikachu','Charizard','Squirtle','Bulbasaur'],
	friends: {
		kanto: ['Togepi', "Starmie"],
		hoenn: ['Geodude','Onyx'],
	},
	talk: function(choosePokemon) {
		console.log('Pikachu!!! I choose you!');
	}

}
console.log(pokemonTrainer);

console.log("Result of dot notation: ");
console.log(pokemonTrainer.name);

console.log("Result of square bracket notation: ")
console.log(pokemonTrainer['pokemon']);

console.log("Result of talk method:");
pokemonTrainer.talk();


/*Constructor for creating a pokemon with the following properties*/

function Pokemon(name, level){

	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	this.tackle = function (target){
		console.log(this.name + " tackled " + target.name);

		target.health -= this.attack;
		console.log(target.name + " health is now reduced to " + target.health)

		if (target.health <= 0){
			target.faint();
		}

	}
	this.faint = function (){
		console.log(this.name + " has fainted ");
	}
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);
